<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Rin
  Date: 26/9/2017
  Time: 12:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>List User</title>
  </head>
  <body>
  User list test:

  <table border="1">
    <tr>
      <th>count</th>
      <th>username</th>
      <th>password</th>
      <th>create-time</th>
    </tr>
    <c:forEach var="user" items="${users}" varStatus="status">
      <tr>
        <td>#${status.count}</td>
        <td><c:out value="${user.username}"/></td>
        <td><c:out value="${user.password}"/></td>
        <td><c:out value="${user.createTime}"/></td>
      </tr>
    </c:forEach>
  </table>
  </body>
</html>
