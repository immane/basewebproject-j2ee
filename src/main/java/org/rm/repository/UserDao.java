package org.rm.repository;

import org.rm.entity.UserEntity;

import java.util.List;

/**
 * Created by Rin on 28/9/2017.
 */
public interface UserDao {
    public void addUser(UserEntity user);
    public String findUserByName(String username);
    public List<UserEntity> list();
    public UserEntity findUserById(Integer id);
    public void updateInfo(UserEntity user);
    public void deleteUserById(Integer id);
}
