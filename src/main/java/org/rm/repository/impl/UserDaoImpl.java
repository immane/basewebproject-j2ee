package org.rm.repository.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.rm.entity.UserEntity;
import org.rm.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Rin on 28/9/2017.
 */
@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao {

    @Autowired private SessionFactory sessionFactory;
    public Session getSession() { return sessionFactory.getCurrentSession(); }

    public void addUser(UserEntity user) {
        this.getSession().saveOrUpdate(user);
    }

    public String findUserByName(String username) {
        return null;
    }

    public List<UserEntity> list() {
        List<UserEntity> userList =
                (List<UserEntity>)this.getSession().createQuery("from UserEntity").list();
        return userList;
    }

    public UserEntity findUserById(Integer id) {
        return null;
    }

    public void updateInfo(UserEntity user) {

    }

    public void deleteUserById(Integer id) {

    }
}
