package org.rm.service.impl;

import org.rm.entity.UserEntity;
import org.rm.repository.UserDao;
import org.rm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Rin on 29/9/2017.
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    public List<UserEntity> list() {
        return userDao.list();
    }
}
