package org.rm.service.impl;

import org.rm.service.TestService;
import org.springframework.stereotype.Service;

/**
 * Created by Rin on 26/9/2017.
 */
@Service
public class TestServiceImpl implements TestService {
    public String test() {
        return "test";
    }
}