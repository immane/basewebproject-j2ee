package org.rm.service;

import org.rm.entity.UserEntity;
import java.util.List;

/**
 * Created by Rin on 29/9/2017.
 */
public interface UserService {
    public List<UserEntity> list();
}
