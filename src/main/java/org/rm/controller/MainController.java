/**
 * Created by Rin on 26/9/2017.
 */
package org.rm.controller;

import org.rm.entity.UserEntity;
import org.rm.service.TestService;
import org.rm.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private TestService testService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public String test() {
        return "test";
    }

    @RequestMapping(value = "springtest", method = RequestMethod.GET)
    public String springTest() {

        return testService.test();
    }

    @RequestMapping(value="user-list.do", method=RequestMethod.GET)
    public String getUsers(Model model) {
        try {
            List<UserEntity> list;
            list = userService.list();
            model.addAttribute("users", list);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "user-list";
    }
}