/**
 * Created by Rin on 27/9/2017.
 */
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.rm.entity.UserEntity;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;


public class DAOTest {
    Configuration config = null;
    SessionFactory sessionFactory = null;
    Session session = null;
    Transaction tx = null;

    @Before
    public void init() {
        config = new Configuration().configure();
        sessionFactory = config.buildSessionFactory();
        session = sessionFactory.openSession();
        tx = session.beginTransaction();
    }

    @Test
    public void insert() {
        UserEntity ue = new UserEntity();
        ue.setUsername("Rin");
        ue.setPassword("123456");
        ue.setEmail("Rin@rm-s.org");
        ue.setCreateTime((java.sql.Date) new Date());
        session.save(ue);
        tx.commit();
    }

    @Test
    public void update() {
        UserEntity user = (UserEntity) session.get(UserEntity.class, new Integer(5));
        user.setUsername("Kana");
        session.update(user);
        tx.commit();
        session.close();
    }

    @Test
    public void getById() {
        UserEntity user = (UserEntity) session.get(UserEntity.class, new Integer(2));
        tx.commit();
        session.close();
        System.out.println("ID:" + user.getId() + "; Username: " + user.getUsername() +
                "; Password: " + user.getPassword() + "; Email: " + user.getEmail());
    }

    @Test
    public void delete() {
        UserEntity user = (UserEntity) session.get(UserEntity.class, new Integer(4));
        session.delete(user);
        tx.commit();
        session.close();
    }
}
